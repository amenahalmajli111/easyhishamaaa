package com.creativpoints.easygate.di

import com.creativpoints.easygate.data.datasourse.airport.RemoteAirportsDataSource
import com.creativpoints.easygate.data.datasourse.article.RemoteArticleDataSource
import com.creativpoints.easygate.data.datasourse.categories.RemoteCategoriesDataSource
import com.creativpoints.easygate.data.datasourse.continents.RemoteContinentsDataSource
import com.creativpoints.easygate.data.datasourse.countries.RemoteCountriesDataSource
import com.creativpoints.easygate.data.datasourse.currency.RemoteCurrencyDataSource
import com.creativpoints.easygate.data.datasourse.getexhibitions.RemoteGetExhibitionsDataSource
import com.creativpoints.easygate.data.datasourse.location.RemoteLocationDataSource
import com.creativpoints.easygate.data.datasourse.register.RemoteRegisterDataSource
import com.creativpoints.easygate.data.datasourse.terminal.RemoteTerminalDataSource
import com.creativpoints.easygate.data.datasourse.timezone.RemoteTimeZoneDataSource
import com.creativpoints.easygate.data.datasourse.weather.RemoteWeatherDataSource
import com.creativpoints.easygate.data.repository.*
import com.creativpoints.easygate.di.qualifiers.Main
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataSourceModule {


    @Provides
    fun provideCategoriesDataSources(@Main retrofit: Retrofit): RemoteCategoriesDataSource {
        return retrofit.create(RemoteCategoriesDataSource::class.java)
    }

    @Provides
    fun provideTerminalDataSources(@Main retrofit: Retrofit): RemoteTerminalDataSource {
        return retrofit.create(RemoteTerminalDataSource::class.java)
    }

    @Provides
    fun provideAirportDataSources(@Main retrofit: Retrofit): RemoteAirportsDataSource {
        return retrofit.create(RemoteAirportsDataSource::class.java)
    }

    @Provides
    fun provideArticleDataSources(@Main retrofit: Retrofit): RemoteArticleDataSource {
        return retrofit.create(RemoteArticleDataSource::class.java)
    }

    @Provides
    fun provideContinentDataSources(@Main retrofit: Retrofit): RemoteContinentsDataSource {
        return retrofit.create(RemoteContinentsDataSource::class.java)
    }

    @Provides
    fun provideCountriesDataSources(@Main retrofit: Retrofit): RemoteCountriesDataSource {
        return retrofit.create(RemoteCountriesDataSource::class.java)
    }

    @Provides
    fun provideCurrencyDataSources(@Main retrofit: Retrofit): RemoteCurrencyDataSource {
        return retrofit.create(RemoteCurrencyDataSource::class.java)
    }

    @Provides
    fun provideGetExhibitionsDataSources(@Main retrofit: Retrofit): RemoteGetExhibitionsDataSource {
        return retrofit.create(RemoteGetExhibitionsDataSource::class.java)
    }

    @Provides
    fun provideLocationDataSources(@Main retrofit: Retrofit): RemoteLocationDataSource {
        return retrofit.create(RemoteLocationDataSource::class.java)
    }

    @Provides
    fun provideRegisterDataSources(@Main retrofit: Retrofit): RemoteRegisterDataSource {
        return retrofit.create(RemoteRegisterDataSource::class.java)
    }

    @Provides
    fun provideTimeZoneDataSources(@Main retrofit: Retrofit): RemoteTimeZoneDataSource {
        return retrofit.create(RemoteTimeZoneDataSource::class.java)
    }

    @Provides
    fun provideWeatherDataSources(@Main retrofit: Retrofit): RemoteWeatherDataSource {
        return retrofit.create(RemoteWeatherDataSource::class.java)
    }

    @Singleton
    @Provides
    fun provideCategoryRepository(remoteCategoriesDataSource: RemoteCategoriesDataSource): CategoryRepository {
        return CategoryRepository(remoteCategoriesDataSource)
    }

    @Singleton
    @Provides
    fun provideTerminalRepository(remoteTerminalDataSource: RemoteTerminalDataSource): TerminalRepository {
        return TerminalRepository(remoteTerminalDataSource)
    }

    @Singleton
    @Provides
    fun provideAirportRepository(remoteAirportsDataSource: RemoteAirportsDataSource): AirportsRepository {
        return AirportsRepository(remoteAirportsDataSource)
    }

    @Singleton
    @Provides
    fun provideArticleRepository(remoteArticleDataSource: RemoteArticleDataSource): ArticleRepository {
        return ArticleRepository(remoteArticleDataSource)
    }


    @Singleton
    @Provides
    fun provideCurrencyRepository(remoteCurrencyDataSource: RemoteCurrencyDataSource): CurrencyRepository {
        return CurrencyRepository(remoteCurrencyDataSource)
    }


    @Singleton
    @Provides
    fun provideTimeZoneRepository(remoteTimeZoneDataSource: RemoteTimeZoneDataSource): TimeZoneRepository {
        return TimeZoneRepository(remoteTimeZoneDataSource)
    }

    @Singleton
    @Provides
    fun provideWeatherRepository(remoteWeatherDataSource: RemoteWeatherDataSource): WeatherRepository {
        return WeatherRepository(remoteWeatherDataSource)
    }
}
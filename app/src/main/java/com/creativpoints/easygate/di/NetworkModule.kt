package com.creativpoints.easygate.di

import com.creativpoints.easygate.di.qualifiers.Currency
import com.creativpoints.easygate.di.qualifiers.Main
import com.serjltt.moshi.adapters.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    private const val TIMEOUT_MINUTES = 1L

    @Provides
    @Singleton
    fun provideOkHttp(): OkHttpClient {

        val builder = OkHttpClient.Builder()
            .readTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .writeTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)
            .connectTimeout(TIMEOUT_MINUTES, TimeUnit.MINUTES)

        return builder.build()
    }

    @Main
    @Provides
    @Singleton
    fun provideMainRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://backend.easygate.app/api/")
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }


    @Currency
    @Provides
    @Singleton
    fun provideCurrencyRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl("")
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideConverterFactory(moshi: Moshi): Converter.Factory {
        return MoshiConverterFactory.create(moshi)
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        val builder = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(KotlinJsonAdapterFactory())
            .add(SerializeNulls.ADAPTER_FACTORY)
            .add(FirstElement.ADAPTER_FACTORY)
            .add(LastElement.ADAPTER_FACTORY)
            .add(ElementAt.ADAPTER_FACTORY)
            .add(FallbackOnNull.ADAPTER_FACTORY)
            .add(FallbackEnum.ADAPTER_FACTORY)
            .add(Wrapped.ADAPTER_FACTORY)
            .add(SerializeOnly.ADAPTER_FACTORY)
            .add(DeserializeOnly.ADAPTER_FACTORY)
            .add(SerializeOnlyNonEmpty.ADAPTER_FACTORY)
        return builder.build()
    }


}
package com.creativpoints.easygate.data.model.airports

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class AirportsLocationResponse(

    @Json(name = "id")
    var id: Int,

    @Json(name = "name")
    var nameEn: String,

    @Json(name = "name_ar")
    var nameAr: String,

    @Json(name = "city")
    var cityEn: String,

    @Json(name = "city_ar")
    var cityAr: String? = null,

    @Json(name = "latitude")
    var latitude: String,

    @Json(name = "longitude")
    var longitude: String,

    ) : Parcelable {

    fun name(prefs: Prefs): String {
        return if (prefs.language == "en") nameEn else nameAr
    }

    fun city(prefs: Prefs): String {
        return if (prefs.language == "en") cityEn else cityAr ?: cityEn
    }

}


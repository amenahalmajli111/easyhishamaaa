package com.creativpoints.easygate.data.datasourse.currency

import com.creativpoints.easygate.data.model.CurrencyResponse
import retrofit2.http.GET
import retrofit2.http.Url

interface RemoteCurrencyDataSource {

    @GET
    suspend fun convert(@Url url: String): CurrencyResponse

}
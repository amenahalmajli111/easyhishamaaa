package com.creativpoints.easygate.data.datasourse.getexhibitions

import com.creativpoints.easygate.data.model.AllCategoriesExhibitionsResponse
import com.creativpoints.easygate.data.model.BaseWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteGetExhibitionsDataSource {

    @GET("getAllCategoriesByExhibitions")
suspend fun getByAllExhibitions():BaseWrapper<List<AllCategoriesExhibitionsResponse>>

    @GET("getArticlesCategoryByExhibitions")
    suspend fun getByCatId(
        @Query("cat_id")airportId:Int)
    :BaseWrapper<List<AllCategoriesExhibitionsResponse>>

    @GET("getArticleInfoExhibition")
    suspend fun getByArticleId(
        @Query("article_id")articleId:Int)
            :BaseWrapper<List<AllCategoriesExhibitionsResponse>>

}
package com.creativpoints.easygate.data.datasourse.continents

import com.creativpoints.easygate.data.model.BaseWrapper
import com.creativpoints.easygate.data.model.ContinentResponse
import retrofit2.http.GET

interface RemoteContinentsDataSource {

    @GET("continents")
    suspend fun getContinent(): BaseWrapper<List<ContinentResponse>>

}
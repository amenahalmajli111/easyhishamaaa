package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.data.datasourse.categories.RemoteCategoriesDataSource
import javax.inject.Inject

class CategoryRepository
@Inject
constructor(private val remoteCategoriesDataSource: RemoteCategoriesDataSource) {

    suspend fun getCategoriesByTerminalId(terminalId:Int) =
        remoteCategoriesDataSource.getCategoriesByTerminalId(terminalId).data


}
package com.creativpoints.easygate.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CurrencyResponse(

    @Json(name = "result")
    var result: Double

)

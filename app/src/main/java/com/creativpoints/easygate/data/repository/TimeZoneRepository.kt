package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.data.datasourse.timezone.RemoteTimeZoneDataSource
import javax.inject.Inject

class TimeZoneRepository
@Inject constructor(private val remoteTimeZoneDataSource: RemoteTimeZoneDataSource) {

    suspend fun getByLatLong(lat: String, long: String): String {
        val baseUrl = "http://api.timezonedb.com/v2.1/"
        val fullUrl = "${baseUrl}get-time-zone?key=TL33X8W59B3D&format=json&lat=$lat&lng=$long&by=position"


        return remoteTimeZoneDataSource.getByLatLong(fullUrl)

    }
}


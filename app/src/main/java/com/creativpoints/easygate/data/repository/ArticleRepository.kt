package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.R
import com.creativpoints.easygate.data.datasourse.article.RemoteArticleDataSource
import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.data.model.article.InformationLocalResponse
import javax.inject.Inject

class ArticleRepository
@Inject
constructor(private val remoteArticleDataSource: RemoteArticleDataSource) {


    suspend fun getArticleInfoId(id: Int): ArticleInfoResponse {


        val articleInfo = remoteArticleDataSource.getArticleInfoId(id).data[0]
        val list: MutableList<InformationLocalResponse> = mutableListOf()

        list.add(
            InformationLocalResponse(
                image = R.drawable.ic_baseline_phone_in_talk_24,
                title = articleInfo.tel,
                color = R.color.tel,
                "tel"
            )
        )
        list.add(
            InformationLocalResponse(
                image = R.drawable.ic_baseline_fax_24,
                title = articleInfo.fax,
                color = R.color.fax,
                "fax"
            )
        )
        list.add(
            InformationLocalResponse(
                image = R.drawable.ic_baseline_email_24,
                title = articleInfo.email,
                color = R.color.email,
                "email"
            )
        )
        list.add(
            InformationLocalResponse(
                image = R.drawable.ic_baseline_language_24,
                title = articleInfo.website,
                color = R.color.website,
                "website"
            )
        )
        list.add(
            InformationLocalResponse(
                image = R.drawable.ic_baseline_location_on_24,
                title = articleInfo.location,
                color = R.color.location,
                "address"
            )
        )



        articleInfo.infoList = list.filter { it.title != "N/A"}

        return articleInfo

    }


    suspend fun getArticleByCategory(categoryId: Int, terminalId: Int) =
        remoteArticleDataSource.getArticleByCategory(categoryId, terminalId).data



}




package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.data.datasourse.currency.RemoteCurrencyDataSource
import javax.inject.Inject

class CurrencyRepository
@Inject constructor(private val remoteCurrencyDataSource: RemoteCurrencyDataSource) {

    suspend fun getConvert(from: String, to: String, amount: String): Double {

        val baseUrl = "https://api.apilayer.com/fixer/"
        val fullUrl = "${baseUrl}convert?to=$to&from=$from&amount=$amount&apikey=$API_KEY"

        return remoteCurrencyDataSource.convert(fullUrl).result.toDouble()

    }

    companion object {
        const val API_KEY =
            "WloaRSipPhzazv7gpA0RMeGq0KqMFseB&fbclid=IwAR1fJ_oU5n8O4AXXTV9ICa-WoI38xxgbOxVIhn2_HiNVqWTdtJdfGho2qo4"
    }
}

package com.creativpoints.easygate.data.datasourse.timezone

import com.serjltt.moshi.adapters.Wrapped
import retrofit2.http.GET
import retrofit2.http.Url

interface RemoteTimeZoneDataSource {
    @GET
    @Wrapped(path = ["formatted"])
    suspend fun getByLatLong(@Url url: String): String
}
package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.data.datasourse.airport.RemoteAirportsDataSource
import javax.inject.Inject

class AirportsRepository @Inject
constructor(private val remoteAirportsDataSource: RemoteAirportsDataSource) {

    suspend fun getAirportsLocation() = remoteAirportsDataSource.getAirportsLocation().data

    suspend fun getById(airportId: Int) = remoteAirportsDataSource.getById(airportId).data

    suspend fun getBySearch(query: String) = remoteAirportsDataSource.getBySearch(query).data



}
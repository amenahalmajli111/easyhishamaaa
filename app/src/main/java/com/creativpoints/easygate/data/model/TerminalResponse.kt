package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class TerminalResponse(
    @Json(name = "id")
    var id: Int,

    @Json(name = "title")
    var titleEn: String,

    @Json(name = "title_ar")
    var titleAr: String,

    @Json(name = "parent")
    var parent: String?=null,

    @Json(name = "airport_id")
    var airportId: String,

    @Json(name = "image_url")
    var imageUrl:String,

    @Json(name = "create_at")
    var createAt:String?=null,

    ):Parcelable {

    fun title(prefs: Prefs): String {
        return if (prefs.language == "en") titleEn else titleAr
    }

}

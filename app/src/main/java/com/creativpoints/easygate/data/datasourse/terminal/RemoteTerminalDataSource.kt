package com.creativpoints.easygate.data.datasourse.terminal

import com.creativpoints.easygate.data.model.BaseWrapper
import com.creativpoints.easygate.data.model.TerminalResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteTerminalDataSource {

    @GET("terminalsByAirport")
    suspend fun getByAirportId(@Query("airport_id") airportId: Int):
            BaseWrapper<List<TerminalResponse>>
}
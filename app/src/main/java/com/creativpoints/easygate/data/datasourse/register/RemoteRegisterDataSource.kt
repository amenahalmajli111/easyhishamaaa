package com.creativpoints.easygate.data.datasourse.register

import com.creativpoints.easygate.data.model.BaseWrapper
import com.creativpoints.easygate.data.model.RegisterResponse
import retrofit2.http.Field
import retrofit2.http.POST

interface RemoteRegisterDataSource {

    @POST("add_token")
    suspend fun sendToken(
                @Field ("token") token:String,
                 @Field("os") os:String
    ) :BaseWrapper<List<RegisterResponse>>
}
package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class RegisterResponse(

    @Json(name = "uid")
    var uid:String,

    @Json(name = "status")
     var status:String,

    @Json(name = "code")
    var code:Int,

):Parcelable

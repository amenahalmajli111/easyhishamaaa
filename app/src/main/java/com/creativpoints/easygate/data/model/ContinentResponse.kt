package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class ContinentResponse(
    @Json(name = "id")
    var id: Int,

    @Json(name = "continent_name")
    var continentName: String,

    @Json(name = "created_at")
    var createdAt: String,

    @Json(name = "updated_at")
    var updatedAt: String,

    @Json(name = "continent_name_ar")
    var continentNameAr: String,

    ) : Parcelable

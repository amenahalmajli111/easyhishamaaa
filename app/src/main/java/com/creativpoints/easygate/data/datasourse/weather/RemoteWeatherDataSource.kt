package com.creativpoints.easygate.data.datasourse.weather

import com.serjltt.moshi.adapters.Wrapped
import retrofit2.http.GET
import retrofit2.http.Url

interface RemoteWeatherDataSource {

    @GET
    @Wrapped(path = ["main","temp"])
    suspend fun getWeather(@Url url: String): Float

}
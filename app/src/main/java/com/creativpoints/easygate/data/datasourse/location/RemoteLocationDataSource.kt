package com.creativpoints.easygate.data.datasourse.location

import com.creativpoints.easygate.data.model.BaseWrapper
import com.creativpoints.easygate.data.model.NotificationResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteLocationDataSource {


   @GET("send_notification")
   suspend fun sendNotification(
       @Query("latitude") latitude:Double,
       @Query("longitude")longitude:Double,
       @Query("uid")userId:Double
   ):BaseWrapper<List<NotificationResponse>>


}
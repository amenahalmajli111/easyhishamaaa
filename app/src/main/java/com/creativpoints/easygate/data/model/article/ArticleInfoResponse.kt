package com.creativpoints.easygate.data.model.article

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class ArticleInfoResponse(
    @Json(name = "id")
    var id: Int,

    @Json(name = "title")
    var titleEn: String,

    @Json(name = "title_ar")
    var titleAr: String,

    @Json(name = "body")
    var body: String? = null,

    @Json(name = "image_url")
    var imageUrl: String,

    @Json(name = "Email")
    var email: String,

    @Json(name = "Fax")
    var fax: String,

    @Json(name = "Tel")
    var tel: String,

    @Json(name = "Location")
    var location: String,

    @Json(name = "Website")
    var website: String,

    @Json(name = "airport")
    var airportResponse: AirportResponse,

    @Json(name = "airport_details")
    var airportDetailResponses: AirportDetailsResponse? = null,

    var infoList: List<InformationLocalResponse>? = null,

    ) : Parcelable {

    fun title(prefs: Prefs): String {
        return if (prefs.language == "en") titleEn else titleAr
    }

}




package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class TimeZoneResponse(
    @Json(name = "status")
    var status:  String,

    @Json(name = "message")
    var message:  String,

    @Json(name = "countryCode")
    var countryCode:  String,

    @Json(name = "countryName")
    var countryName:  String,

    @Json(name = "zoneName")
    var zoneName:  String,

    @Json(name = "abbreviation")
    var abbreviation:  String,

    @Json(name = "gmtOffset")
    var gmtOffset:  Int,

    @Json(name = "dst")
    var dst:  String,

    @Json(name = "zoneStart")
    var zoneStart:  Int,

    @Json(name = "zoneEnd")
    var zoneEnd:  Int,

    @Json(name = "nextAbbreviation")
    var nextAbbreviation:  String,

    @Json(name = "timestamp")
    var timestamp:  Int,

    @Json(name = "formatted")
    var formatted:  String,

):Parcelable

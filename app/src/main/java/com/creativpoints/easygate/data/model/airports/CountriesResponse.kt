package com.creativpoints.easygate.data.model.airports

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class CountriesResponse(
    @Json(name = "id")
    var id: Int,

    @Json(name = "country_name")
    var countryNameEn: String,

    @Json(name = "country_code")
    var countryCode: String,

    @Json(name = "country_name_ar")
    var countryNameAr: String,

    @Json(name = "country_flag")
    var countryFlag: String,

    @Json(name = "continent_id")
    var continentId: String

) : Parcelable {

    fun country(prefs: Prefs): String {
        return if (prefs.language == "en") countryNameEn else countryNameAr
    }


}

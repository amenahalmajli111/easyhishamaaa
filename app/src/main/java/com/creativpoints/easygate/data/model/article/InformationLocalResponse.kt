package com.creativpoints.easygate.data.model.article

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class InformationLocalResponse(
    var image: Int,
    var title: String,
    var color: Int,
    var slug: String

) : Parcelable

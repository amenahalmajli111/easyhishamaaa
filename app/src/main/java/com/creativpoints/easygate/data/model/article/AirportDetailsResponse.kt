package com.creativpoints.easygate.data.model.article

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class AirportDetailsResponse(
    @Json(name = "id")
    var id: Int,

    @Json(name = "hours")
    var hours: String,

    @Json(name = "atc")
    var atc: String,

    @Json(name = "city")
    var cityEn: String,

    @Json(name = "timezone")
    var timezone: String,

    @Json(name = "latitude")
    var latitude: String,

    @Json(name = "longitude")
    var longitude: String,

    @Json(name = "air_id")
    var airId: String,

    @Json(name = "created_at")
    var createdAt: String? = null,

    @Json(name = "updated_at")
    var updatedAt: String,

    @Json(name = "city_code")
    var cityCode: String,

    @Json(name = "city_ar")
    var cityAr: String? = null,

    ) : Parcelable {

    fun city(prefs: Prefs): String? {
        return if (prefs.language == "en") cityEn else cityAr
    }


}
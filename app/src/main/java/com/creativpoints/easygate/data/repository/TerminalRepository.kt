package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.data.datasourse.terminal.RemoteTerminalDataSource
import javax.inject.Inject

class TerminalRepository
@Inject constructor(private val remoteTerminalDataSource: RemoteTerminalDataSource)
{
    suspend fun getByAirportId(airportId: Int) = remoteTerminalDataSource.getByAirportId(airportId).data
}
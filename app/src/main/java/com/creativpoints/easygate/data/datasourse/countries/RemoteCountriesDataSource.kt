package com.creativpoints.easygate.data.datasourse.countries

import com.creativpoints.easygate.data.model.BaseWrapper
import com.creativpoints.easygate.data.model.airports.CountriesResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteCountriesDataSource {

    @GET("countries/{continentId}")
    suspend fun listByContinentId(
        @Path("continentId")continentId:Int):
            BaseWrapper<List<CountriesResponse>>

    @GET("countries/{continentId}")
    suspend fun searchByContinent(
        @Query("continent_id") continentId:Int,
        @Query("key") key:String):
            BaseWrapper<List<CountriesResponse>>

}
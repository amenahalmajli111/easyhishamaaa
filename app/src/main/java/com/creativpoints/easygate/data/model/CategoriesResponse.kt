package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class CategoriesResponse(
    @Json(name = "id")
    var id: Int,

    @Json(name = "title_en")
    var titleEn: String,

    @Json(name = "image_url")
    var imageUrl: String,

    @Json(name = "title_ar")
    var titleAr: String,

    @Json(name = "desc_en")
    var descEn: String? = null,

    @Json(name = "desc_ar")
    var descAr: String? = null,

    @Json(name = "parent")
    var parent: String? = "",

    @Json(name = "no_field")
    var noField: String,

    @Json(name = "kind")
    var kind: String,

    ) : Parcelable {

    fun title(prefs: Prefs): String {
        return if (prefs.language == "en") titleEn else titleAr
    }

    fun desc(prefs: Prefs): String? {
        return if (prefs.language == "en") descEn else descAr
    }

}

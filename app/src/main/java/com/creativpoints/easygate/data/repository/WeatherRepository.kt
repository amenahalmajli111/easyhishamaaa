package com.creativpoints.easygate.data.repository

import com.creativpoints.easygate.data.datasourse.weather.RemoteWeatherDataSource
import javax.inject.Inject

class WeatherRepository
@Inject constructor(private val remoteWeatherDataSource: RemoteWeatherDataSource) {

    suspend fun getWeather(lat: String, long: String): Float {
        val baseUrl = "http://api.openweathermap.org/data/2.5/"
        val fullUrl = "${baseUrl}weather?lat=$lat&appid=d9e98aac5a1345869b2db64538527f78&lon=$long&units=metric"

        return remoteWeatherDataSource.getWeather(fullUrl)
    }
}


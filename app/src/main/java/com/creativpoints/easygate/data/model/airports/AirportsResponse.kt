package com.creativpoints.easygate.data.model.airports

import android.os.Parcelable
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.article.AirportDetailsResponse
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class AirportsResponse(

    @Json(name = "id")
    var id: Int,

    @Json(name = "name")
    var nameEn: String,

    @Json(name = "name_ar")
    var nameAr: String,

    @Json(name = "Location")
    var location: String,

    @Json(name = "ICAO")
    var icon: String,

    @Json(name = "IATA")
    var iata: String?,

    @Json(name = "ImageUrl")
    var imageUrl: String?,

    @Json(name = "ImageMap")
    var imageMap: String,

    @Json(name = "Country_Id")
    var countryId: String,

    @Json(name = "approved")
    var approved: String,

    @Json(name = "countries")
    var countries: CountriesResponse,

    @Json(name = "airport_details")
    var airportDetails: AirportDetailsResponse,

    @Transient
    var formatted: String = "null",

    @Transient
    var temp: Float = 0f

) : Parcelable {
    fun name(prefs: Prefs): String {
        return if (prefs.language == "en") nameEn else nameAr
    }

}

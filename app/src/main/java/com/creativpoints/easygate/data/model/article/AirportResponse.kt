package com.creativpoints.easygate.data.model.article

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class AirportResponse(
    @Json(name="id")
    var id: Int,

    @Json(name="Location")
    var location:String,

    @Json(name="ImageUrl")
    var imageUrl:String,

    @Json(name="ImageMap")
    var imageMap:String,

    @Json(name="ICAO")
    var icao:String,

    @Json(name="name")
    var name:String,

    @Json(name="IATA")
    var iata:String,

    ): Parcelable
package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class WeatherResponse(

    @Json(name = "temp")
    var temp: Float,

    @Json(name = "pressure")
    var pressure: Float,

   @Json(name = "humidity")
   var humidity: Int,

   @Json(name = "temp_min")
    var tempMin: Float,

  @Json(name = "temp_max")
  var tempMax: Float,

  @Json(name = "sea_level")
  var seaLevel: Float,

  @Json(name = "grnd_level")
  var grndLevel: Float

):Parcelable

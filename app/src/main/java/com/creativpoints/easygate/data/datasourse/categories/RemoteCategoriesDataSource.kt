package com.creativpoints.easygate.data.datasourse.categories

import com.creativpoints.easygate.data.model.CategoriesResponse
import com.creativpoints.easygate.data.model.BaseWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface  RemoteCategoriesDataSource {


    @GET("categoriesByTerminal")
    suspend fun getCategoriesByTerminalId(
        @Query("terminal_id") terminalId: Int,
    ):BaseWrapper<List<CategoriesResponse>>



    @GET("categoriesByCity")
    suspend fun getCategoriesByCityName(
        @Query("city")city:String)
            :BaseWrapper<List<CategoriesResponse>>


}
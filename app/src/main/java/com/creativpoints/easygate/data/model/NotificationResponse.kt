package com.creativpoints.easygate.data.model

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.parcelize.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class NotificationResponse(
    @Json(name = "status")
    var status: String,

    @Json(name = "message")
    var message: String,

    @Json(name = "code")
    var code: Int,

    ):Parcelable

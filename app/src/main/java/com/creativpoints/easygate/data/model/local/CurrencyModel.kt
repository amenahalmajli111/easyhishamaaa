package com.creativpoints.easygate.data.model.local


data class CurrencyModel(val iso: String, val name: Int, val flag: Int)

package com.creativpoints.easygate.data.datasourse.article

import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.data.model.BaseWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteArticleDataSource {

    @GET("getArticleInfoByCity")
    suspend fun getArticleInfoByCity(
        @Query("article_id") id: Int
    )
            : BaseWrapper<ArticleInfoResponse>

    @GET("getArticleInfo")
    suspend fun getArticleInfoId(
        @Query("article_id") id: Int
    ): BaseWrapper<List<ArticleInfoResponse>>


    @GET("articlesByCategory")
    suspend fun getArticleByCategory(
        @Query("cat_id") categoryId: Int,
        @Query("terminal_id") terminalId: Int
    ): BaseWrapper<List<ArticleInfoResponse>>


    @GET("getCargoTerminal")
    suspend fun getCarGoTerminal(
        @Query("air_id") airportId: Int,
        @Query("cat_id") categoryId: Int
    ): BaseWrapper<List<ArticleInfoResponse>>


}
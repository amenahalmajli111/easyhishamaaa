package com.creativpoints.easygate.data.datasourse.airport

import com.creativpoints.easygate.data.model.BaseWrapper

import com.creativpoints.easygate.data.model.airports.AirportsLocationResponse
import com.creativpoints.easygate.data.model.airports.AirportsResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RemoteAirportsDataSource {

    @GET("getAirportsLocations")
    suspend fun getAirportsLocation(): BaseWrapper<List<AirportsLocationResponse>>

    @GET("airports/{id}")
    suspend fun getById(@Path("id") Id: Int)
            : BaseWrapper<AirportsResponse>

    @GET("airports/search")
    suspend fun getBySearch(
        @Query("key") key: String
    ): BaseWrapper<List<AirportsResponse>>

    @GET("airports/getByCountry")
    suspend fun getByCountry(
        @Query("country_id") countryId: String
    )
            : BaseWrapper<List<AirportsResponse>>

    @GET("airports/searchByContinent")
    suspend fun getByContinent(
        @Query("continent_id") continentId: String,
        @Query("key") key: String
    ): BaseWrapper<List<AirportsResponse>>

}
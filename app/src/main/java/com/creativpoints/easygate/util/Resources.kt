package com.creativpoints.easygate.util

import android.content.Context
import androidx.annotation.DrawableRes


fun String.toStringResource(context: Context) = context.resources.getIdentifier(
    this,
    "string",
    context.packageName
)


@DrawableRes
fun String.toDrawableResource(context: Context) = context.resources.getIdentifier(
    this,
    "drawable",
    context.packageName
)

package com.creativpoints.easygate.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class DateUtils {

    public static String getDate(String date) {
        return stringToString("yyyy-MM-dd KK:mm:ss", "yyyy-MM-dd", date);
    }

    public static String getTime(String date) {
        return stringToString("yyyy-MM-dd KK:mm:ss", "kk:mm ", date);
    }

    private static String stringToString(String format, String output, String strDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        Date date = null;
        try {
            date = dateFormat.parse(strDate);
        } catch (Exception ignored) {
        }
        dateFormat.applyPattern(output);
        return date != null ? dateFormat.format(date) : "";
    }

    public static String GTMToString() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("kk:mm", Locale.ENGLISH);
        date.setTimeZone(TimeZone.getTimeZone("GMT"));
        return date.format(currentLocalTime);
    }
}

package com.creativpoints.easygate.ui.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.airports.AirportsResponse
import com.creativpoints.easygate.databinding.RowSearchAirportBinding

class SearchAdapter(
    private val prefs: Prefs,
    private val onItemClickListener: (id:Int) -> Unit
) : ListAdapter<AirportsResponse, SearchAdapter.SearchViewHolder>(DiffCallback()) {

    inner class SearchViewHolder(private val binding: RowSearchAirportBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(airportResponse: AirportsResponse) {
            binding.prefs = prefs
            binding.model = airportResponse
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val binding =
            RowSearchAirportBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(getItem(position))

        holder.itemView.setOnClickListener {
            onItemClickListener(getItem(position).id)
        }
    }


    class DiffCallback : DiffUtil.ItemCallback<AirportsResponse>() {
        override fun areItemsTheSame(
            oldItem: AirportsResponse,
            newItem: AirportsResponse
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: AirportsResponse,
            newItem: AirportsResponse
        ): Boolean {
            return oldItem == newItem
        }

    }

}
package com.creativpoints.easygate.ui.teminal.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.TerminalResponse
import com.creativpoints.easygate.databinding.RowTerminalBinding

class TerminalAdapter(val listener: OnItemClickListener, val prefs: Prefs) :
    ListAdapter<TerminalResponse, TerminalAdapter.TerminalViewHolder>(DiffCallback()) {

    inner class TerminalViewHolder(private val binding: RowTerminalBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(terminalResponse: TerminalResponse) {
            binding.prefs = prefs
            binding.model = terminalResponse
            binding.executePendingBindings()

            binding.cardView.setOnClickListener {
                listener.onItemClick(terminalResponse)
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TerminalViewHolder {
        val binding =
            RowTerminalBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TerminalViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TerminalViewHolder, position: Int) {
        val terminal = getItem(position)
        holder.bind(terminal)
    }


    class DiffCallback : DiffUtil.ItemCallback<TerminalResponse>() {
        override fun areItemsTheSame(
            oldItem: TerminalResponse,
            newItem: TerminalResponse
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: TerminalResponse,
            newItem: TerminalResponse
        ): Boolean {
            return oldItem == newItem
        }
    }


    interface OnItemClickListener {
        fun onItemClick(terminalResponse: TerminalResponse)
    }

}
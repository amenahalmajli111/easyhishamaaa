package com.creativpoints.easygate.ui.infodesk

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.creativpoints.easygate.databinding.FragmentInfoDeskBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InformationDeskFragment : Fragment() {
    private lateinit var binding: FragmentInfoDeskBinding
    private val viewModel: InformationDeskViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentInfoDeskBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRequest()

    }

    private fun initRequest() {

    }


}
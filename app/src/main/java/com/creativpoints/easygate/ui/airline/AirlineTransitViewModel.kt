package com.creativpoints.easygate.ui.airline

import androidx.lifecycle.*
import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.data.repository.ArticleRepository
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class AirlineTransitViewModel @Inject constructor(
    private val articleRepository: ArticleRepository,
) : ViewModel() {


    private val _loadingHomeLiveData = MutableLiveData(false)
    val loadingLiveData: LiveData<Boolean> = _loadingHomeLiveData

    private val _errorSHomeLiveData = MutableLiveData<Exception>()
    val errorLiveData: LiveData<Exception> = _errorSHomeLiveData

    private val _successArticleInfoByTerminalIdLiveData = MutableLiveData<List<ArticleInfoResponse>>()
    val successArticleInfoByTerminalIdLiveData: LiveData<List<ArticleInfoResponse>> =
        _successArticleInfoByTerminalIdLiveData


    fun getArticleInfoById(categoryId: Int, city: String?, terminalId: Int?) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {

                _successArticleInfoByTerminalIdLiveData.postValue(
                    articleRepository.getArticleByCategory(categoryId, terminalId!!)
                )
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
             FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }


}
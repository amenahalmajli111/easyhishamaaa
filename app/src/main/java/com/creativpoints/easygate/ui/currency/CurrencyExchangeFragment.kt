package com.creativpoints.easygate.ui.currency

import android.os.Bundle
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.creativpoints.easygate.R
import com.creativpoints.easygate.data.model.local.CurrencyModel
import com.creativpoints.easygate.databinding.FragmentCurrencyBinding
import com.creativpoints.easygate.ui.currency.dialog.CountryDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CurrencyExchangeFragment : Fragment() {
    private lateinit var binding: FragmentCurrencyBinding
    private val viewModel: CurrencyExchangeViewModel by viewModels()

    private val currencies: List<CurrencyModel> by lazy { viewModel.getCurrencies() }
    private var rotation = 180f



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentCurrencyBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.included.tvToolbar.text = getString(R.string.screen_currency)
        initClick()
        constraintSpinnerAnimation()
        initObserve()
    }

    private fun initObserve() {
        viewModel.successCurrencyLiveData.observe(viewLifecycleOwner, ::convertSuccess)
    }

    private fun convertSuccess(amount: String) {
        binding.tvResultAmount.text = amount
    }


    private fun initClick() {
        binding.btnTransfer.setOnClickListener {
            onConvertClicked()
        }
        binding.cardViewFrom.setOnClickListener {
            CountryDialog(currencies, ::onFromCountrySelected).show(
                requireActivity().supportFragmentManager,
                "MyCustomFragment"
            )
        }

        binding.cardViewTo.setOnClickListener {
            CountryDialog(currencies, ::onToCountrySelected).show(
                requireActivity().supportFragmentManager,
                "MyCustomFragment"
            )
        }

    }

    private fun onFromCountrySelected(currencyModel: CurrencyModel) {
        binding.spinnerFrom.text = currencyModel.iso
    }
    private fun onToCountrySelected(currencyModel: CurrencyModel) {
        binding.spinnerTo.text = currencyModel.iso
    }

    private fun constraintSpinnerAnimation() {
        val startingConstraintSet = ConstraintSet()
        startingConstraintSet.clone(binding.root)

        val finishingConstraintSet = ConstraintSet()
        finishingConstraintSet.clone(context, R.layout.fragment_currency_animation)

        var set = false

        binding.cardViewSwitch.setOnClickListener {
            binding.ivSwap.animate().rotation(rotation).setDuration(500).start()

            rotation = if (rotation == 180f) 0f else 180f

            TransitionManager.beginDelayedTransition(binding.root)
            val constraint = if (set) startingConstraintSet else finishingConstraintSet
            constraint.applyTo(binding.root)
            set = !set

            if (binding.etAmount.length() > 0) {
                it.postDelayed(this::onConvertClicked, 500L)
            }
        }
    }

    private fun onConvertClicked() {
        if (binding.etAmount.length() > 0) {
            val from: String
            val to: String
            if (rotation == 180f) {
                from = binding.spinnerFrom.text.toString()
                to = binding.spinnerTo.text.toString()
            } else {
                from = binding.spinnerTo.text.toString()
                to = binding.spinnerFrom.text.toString()
            }

            viewModel.getCurrency(from, to, binding.etAmount.text.toString())
        } else {
            Toast.makeText(requireContext(), R.string.toast_enter_amount, Toast.LENGTH_SHORT)
                .show()
        }
    }
}





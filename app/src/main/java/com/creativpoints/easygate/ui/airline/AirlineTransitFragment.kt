package com.creativpoints.easygate.ui.airline

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.databinding.FragmentAirlineTransitBinding
import com.creativpoints.easygate.ui.airline.adapter.AirlineTransitAdapter
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class AirlineTransitFragment : Fragment(), AirlineTransitAdapter.OnItemClickListener {

    private lateinit var binding: FragmentAirlineTransitBinding
    private val viewModel: AirlineTransitViewModel by viewModels()
    private lateinit var airlineTransitAdapter: AirlineTransitAdapter
    private val args by navArgs<AirlineTransitFragmentArgs>()

    @Inject
    lateinit var prefs: Prefs

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAirlineTransitBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.included.tvToolbar.text = args.title
        initRequest()
        initAdapter()
        observer()


    }

    private fun initAdapter() {
        airlineTransitAdapter = AirlineTransitAdapter(this)
        binding.recyclerView.adapter = airlineTransitAdapter

    }

    private fun initRequest() {
        viewModel.getArticleInfoById(args.categoryId, args.city, args.terminalId)

    }

    private fun observer() {


        viewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)

        viewModel.errorLiveData.observe(viewLifecycleOwner, ::handleError)


        viewModel.successArticleInfoByTerminalIdLiveData.observe(
            viewLifecycleOwner,
            ::articleByTerminalSuccess
        )

    }


    private fun articleByTerminalSuccess(list: List<ArticleInfoResponse>) {
        airlineTransitAdapter.submitList(list)
    }

    private fun handleLoading(flag: Boolean?) {
        binding.progressbar.isVisible = flag!!

    }

    fun handleError(exception: Exception) {
        FirebaseCrashlytics.getInstance().recordException(exception)

    }

    override fun onItemClick(articleInfoResponse: ArticleInfoResponse) {
        if (args.city != null) {
            val action =
                AirlineTransitFragmentDirections.actionAirlineTransitFragmentToInformationArticleFragment(
                    articleInfoResponse.id,
                    articleInfoResponse.title(prefs)
                )
            findNavController().navigate(action)

        } else {
            val action =
                AirlineTransitFragmentDirections.actionAirlineTransitFragmentToInformationDeskFragment()
            findNavController().navigate(action)

        }


    }
}




package com.creativpoints.easygate.ui.articleinfo

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.data.model.article.InformationLocalResponse
import com.creativpoints.easygate.databinding.FragmentArticleInfoBinding
import com.creativpoints.easygate.ui.articleinfo.adapter.InformationArticleAdapter
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InformationArticleFragment : Fragment(), InformationArticleAdapter.OnItemClickListener {
    private lateinit var binding: FragmentArticleInfoBinding
    private val viewModel: InformationArticleViewModel by viewModels()
    private val args by navArgs<InformationArticleFragmentArgs>()
    private lateinit var informationArticleAdapter: InformationArticleAdapter
    private lateinit var articleInfoResponse: ArticleInfoResponse

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentArticleInfoBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.included.tvToolbar.text = args.title
        initRequest()
        initAdapter()
        observer()
    }

    private fun initAdapter() {
        informationArticleAdapter = InformationArticleAdapter(this, requireContext())
        binding.recyclerView.adapter = informationArticleAdapter
    }

    private fun initRequest() {
        viewModel.getArticleInfo(args.articleId)

    }

    private fun observer() {

        viewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)

        viewModel.errorLiveData.observe(viewLifecycleOwner, ::handleError)

        viewModel.successArticleInfoLiveData.observe(viewLifecycleOwner, ::articleInfoSuccess)
    }

    private fun articleInfoSuccess(articleInfoResponse: ArticleInfoResponse) {
        informationArticleAdapter.submitList(articleInfoResponse.infoList)
        binding.model = articleInfoResponse
    }

    private fun handleLoading(flag: Boolean) {
        binding.progressbar.isVisible = flag

    }

    fun handleError(exception: Exception) {
        FirebaseCrashlytics.getInstance().recordException(exception)

    }

    override fun onItemClick(article: InformationLocalResponse) {

        when (article.slug) {

            "tel" -> {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:" + article.title)
                startActivity(intent)
            }

            "email" -> {
                val intent = Intent(Intent.ACTION_SEND)
                intent.data = Uri.parse("mailto:${article.title}")
                intent.type = "text/plain"
                startActivity(Intent.createChooser(intent, "Select your Email app"))
            }
            "website" -> {
                val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(article.title))
                startActivity(webIntent)
            }

        }


    }


}
package com.creativpoints.easygate.ui.articleinfo

import androidx.lifecycle.*
import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.data.repository.*
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class InformationArticleViewModel @Inject constructor(
    private val articleRepository: ArticleRepository,

    ) : ViewModel() {

    private val _loadingHomeLiveData = MutableLiveData(false)
    val loadingLiveData: LiveData<Boolean> = _loadingHomeLiveData

    private val _errorSHomeLiveData = MutableLiveData<Exception>()
    val errorLiveData: LiveData<Exception> = _errorSHomeLiveData

    private val _successArticleInfoLiveData = MutableLiveData<ArticleInfoResponse>()
    val successArticleInfoLiveData: LiveData<ArticleInfoResponse> = _successArticleInfoLiveData

    fun getArticleInfo(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {
                _successArticleInfoLiveData.postValue(articleRepository.getArticleInfoId(id)
                )
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }

}
package com.creativpoints.easygate.ui.articleinfo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.creativpoints.easygate.data.model.article.InformationLocalResponse
import com.creativpoints.easygate.databinding.RowArticleInfoBinding

class InformationArticleAdapter(val listener: OnItemClickListener, val context: Context) :
    ListAdapter<InformationLocalResponse, InformationArticleAdapter.InformationArticleViewHolder>(
        DiffCallback()) {


    inner class InformationArticleViewHolder(private val binding: RowArticleInfoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(article: InformationLocalResponse) {
            binding.model = article


            binding.cardView.setBackgroundColor(ContextCompat.getColor(context, article.color))
            binding.ivIcon.setImageResource(article.image)

            binding.executePendingBindings()
            binding.cardView.setOnClickListener {
                listener.onItemClick(article)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,
    ): InformationArticleViewHolder {
        val binding =
            RowArticleInfoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return InformationArticleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: InformationArticleViewHolder, position: Int) {
        val article = getItem(position)
        holder.bind(article)
    }

    class DiffCallback : DiffUtil.ItemCallback<InformationLocalResponse>() {
        override fun areItemsTheSame(
            oldItem: InformationLocalResponse,
            newItem: InformationLocalResponse,
        ): Boolean {
            return oldItem.title == newItem.title

        }

        override fun areContentsTheSame(
            oldItem: InformationLocalResponse,
            newItem: InformationLocalResponse,
        ): Boolean {
            return oldItem == newItem
        }

    }

    interface OnItemClickListener {
        fun onItemClick(article: InformationLocalResponse)
    }
}
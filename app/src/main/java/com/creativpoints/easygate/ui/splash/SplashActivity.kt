package com.creativpoints.easygate.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.creativpoints.easygate.databinding.ActivitySplashBinding
import com.creativpoints.easygate.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initHandler()
    }

    private fun intiGoToHome() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun initHandler() {
        val handler = Handler()
        handler.postDelayed({
            intiGoToHome()

        }, 2000)

    }
}
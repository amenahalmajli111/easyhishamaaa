package com.creativpoints.easygate.ui.teminal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.creativpoints.easygate.R
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.TerminalResponse
import com.creativpoints.easygate.databinding.FragmentPassengerTerminalBinding
import com.creativpoints.easygate.ui.teminal.adapter.TerminalAdapter
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PassengerTerminalFragment : Fragment(), TerminalAdapter.OnItemClickListener {

    @Inject
    lateinit var prefs: Prefs

    private lateinit var binding: FragmentPassengerTerminalBinding
    private val viewModel: TerminalViewModel by viewModels()
    private lateinit var terminalAdapter: TerminalAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPassengerTerminalBinding.inflate(layoutInflater)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.included.tvToolbar.text = getString(R.string.screen_passenger_terminal)
//        initRequest()
        initAdapter()
        observer()
    }

    private fun initAdapter() {
        terminalAdapter = TerminalAdapter(this, prefs)
        terminalAdapter.submitList((arguments?.getParcelableArray("Terminals") as? Array<TerminalResponse>?)?.toList())
        binding.recyclerView.adapter = terminalAdapter
    }

    private fun observer() {
        viewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)

        viewModel.errorLiveData.observe(viewLifecycleOwner, ::handleError)

        viewModel.successTerminalLiveData.observe(viewLifecycleOwner, ::terminalSuccess)

    }

    private fun terminalSuccess(list: List<TerminalResponse>) {
        if (list.size == 1) {
            onItemClick(list[0])
            return
        }
        terminalAdapter.submitList(list)
    }

    private fun handleLoading(flag: Boolean?) {
        binding.progressbar.isVisible = flag!!

    }

    fun handleError(exception: Exception) {

        FirebaseCrashlytics.getInstance().recordException(exception)

    }


    override fun onItemClick(terminalResponse: TerminalResponse) {

        val action = PassengerTerminalFragmentDirections.actionPassengerTerminalFragmentToCategoryFragment(
            terminalResponse.id, terminalResponse.airportId.toInt(), ""
        )
        findNavController().navigate(action)
    }

}
package com.creativpoints.easygate.ui.currency.dialog

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.creativpoints.easygate.R
import com.creativpoints.easygate.data.model.local.CurrencyModel
import com.creativpoints.easygate.databinding.DialogCountryBinding
import com.creativpoints.easygate.ui.currency.dialog.adapter.CurrencyAdapter
import java.util.*

class CountryDialog(private val currencies: List<CurrencyModel>, private val onSelected: (CurrencyModel) -> Unit) :
    DialogFragment() {

    private lateinit var binding: DialogCountryBinding
    private lateinit var adapter: CurrencyAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        requireDialog().window?.setBackgroundDrawableResource(R.drawable.round_corner);
        binding = DialogCountryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
        searchOrder()
    }

    private fun initAdapter() {
        adapter = CurrencyAdapter(currencies) {
            onSelected(it)
            dismiss()
        }
        binding.recyclerViewSpecs.adapter = adapter
    }


    override fun onStart() {
        super.onStart()
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        val height = (resources.displayMetrics.heightPixels * 0.75).toInt()
        requireDialog().window?.setLayout(width, height)
    }

    private fun searchOrder() {
        binding.editTextSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(idoOrder: Editable) {
                filter(idoOrder.toString())
            }
        })
    }

    private fun filter(idoOrder: String) {
        val filteredList: MutableList<CurrencyModel> = ArrayList()

        currencies.forEach {
            if (it.iso.lowercase(Locale.getDefault()).contains(idoOrder.lowercase(Locale.getDefault()))) {
                filteredList.add(it)
            }
        }

        adapter.filterList(filteredList)
    }
}

package com.creativpoints.easygate.ui.currency.dialog.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.creativpoints.easygate.data.model.local.CurrencyModel
import com.creativpoints.easygate.databinding.RowCurrencyBinding

class CurrencyAdapter(
    private var currencies: List<CurrencyModel>,
    private val onClick: (CurrencyModel) -> Unit,
) : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        layoutInflater = LayoutInflater.from(recyclerView.context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(RowCurrencyBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currencies[position]
        holder.bind(item)
        holder.itemView.setOnClickListener {
            onClick(item)
        }
    }

    override fun getItemCount(): Int {
        return currencies.size
    }

    fun filterList(filteredList: List<CurrencyModel>) {
        currencies = filteredList
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: RowCurrencyBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(model: CurrencyModel) {
            binding.imageViewRowRateFlag.setImageResource(model.flag)
            binding.currency = model
            binding.executePendingBindings()
        }
    }
}

package com.creativpoints.easygate.ui.home

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.appolica.interactiveinfowindow.InfoWindow
import com.appolica.interactiveinfowindow.InfoWindowManager
import com.appolica.interactiveinfowindow.fragment.MapInfoWindowFragment
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.TerminalResponse
import com.creativpoints.easygate.data.model.airports.AirportsLocationResponse
import com.creativpoints.easygate.data.model.airports.AirportsResponse
import com.creativpoints.easygate.databinding.FragmentHomeBinding
import com.creativpoints.easygate.ui.home.adapter.SearchAdapter
import com.creativpoints.easygate.ui.main.MainActivity
import com.creativpoints.easygate.util.View.doOnDebounceQueryChange
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import com.creativpoints.easygate.R
import com.google.firebase.crashlytics.FirebaseCrashlytics


@AndroidEntryPoint
class HomeFragment : Fragment(), OnMapReadyCallback,
    GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var mMap: GoogleMap
    private lateinit var searchAdapter: SearchAdapter

    private lateinit var terminals: List<TerminalResponse>
    var isSingleTerminal = false

    private lateinit var infoWindowManager: InfoWindowManager


    @Inject
    lateinit var prefs: Prefs

    override fun onDestroyView() {
        super.onDestroyView()
        mMap.clear()
        infoWindowManager.onDestroy()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initMap()
        initClickLanguage()
        init()
        observer()
        onBackPresses()
    }


    private fun initClickLanguage() {
        binding.ivLanguage.setOnClickListener {
            if (prefs.language == "ar") {
                prefs.language = "en"
            } else {
                prefs.language = "ar"
            }
            startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        }
    }

    private fun initMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.mapFragment) as MapInfoWindowFragment

        mapFragment.getMapAsync(this)

        infoWindowManager = mapFragment.infoWindowManager()
        infoWindowManager.setHideOnFling(true)


        binding.etSearch.doOnDebounceQueryChange(viewLifecycleOwner) {
            binding.recyclerView.isVisible = it.isNotBlank()
            if (it.isNotBlank()) viewModel.getSearchAirport(it)

        }
    }


    private fun init() {
        initAdapter()
    }

    private fun initAdapter() {
        searchAdapter = SearchAdapter(prefs, ::onClickSearchAdapter)
        binding.recyclerView.adapter = searchAdapter
    }

    private fun onClickSearchAdapter(airportId: Int) {
        viewModel.getTerminal(airportId, true)
    }


    private fun observer() {
        viewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)
        viewModel.errorLiveData.observe(viewLifecycleOwner, ::handleError)
        viewModel.successCLocationLiveData.observe(viewLifecycleOwner, ::locationSuccess)
        viewModel.successLocationSearchLiveData.observe(viewLifecycleOwner, ::searchAirportSuccess)
        viewModel.successTerminalLiveData.observe(viewLifecycleOwner, ::terminalSuccess)
    }

    private fun getTerminal(airportId: Int) {
        viewModel.getTerminal(airportId)
    }


    private fun terminalSuccess(pair: Pair<List<TerminalResponse>, Boolean>) {
        val list = pair.first
        terminals = list
        isSingleTerminal = list.size == 1
        if (pair.second) {
            openNextScreen()
        }
    }


    private fun locationSuccess(list: List<AirportsLocationResponse>) {

        for (item in list) {

            if (item.cityAr == null) {
                Log.e(
                    "city_issue",
                    "Country ${item.name(prefs)} | cityAr ${item.cityAr}"
                )
            }
            val marker =
                createMarker(
                    item.latitude.toDouble(),
                    item.longitude.toDouble(),
                    item.name(prefs),
                    item.city(prefs)
                )
            marker?.tag = item
        }

    }

    private fun searchAirportSuccess(list: List<AirportsResponse>) {
        binding.recyclerView.isVisible = true
        searchAdapter.submitList(list)
        searchAdapter.notifyDataSetChanged()
    }

    private fun handleLoading(flag: Boolean) {
        binding.progressbar.isVisible = flag

    }

    private fun handleError(exception: Exception) {
        FirebaseCrashlytics.getInstance().recordException(exception)
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(mMap: GoogleMap) {
        this.mMap = mMap
        infoWindowManager.onMapReady(mMap)
        mMap.setOnInfoWindowClickListener(this)
        mMap.setOnMarkerClickListener(this)

        mMap.uiSettings.isZoomControlsEnabled = true

    }

    private fun createMarker(
        latitude: Double,
        longitude: Double,
        title: String?,
        snippet: String?,
    ) = mMap.addMarker(
        MarkerOptions()
            .position(LatLng(latitude, longitude))
            .title(title)
            .snippet(snippet)
    )

    private fun actionToPassengerTerminalFragment(list: List<TerminalResponse>) {
        findNavController().navigate(
            R.id.passengerTerminalFragment, bundleOf(
                "Terminals" to list.toTypedArray()
            )
        )
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        getTerminal((marker.tag as AirportsLocationResponse).id)
        val offsetX = resources.getDimension(R.dimen.marker_offset_x).toInt()
        val offsetY = resources.getDimension(R.dimen.marker_offset_y).toInt()
        val markerSpec = InfoWindow.MarkerSpecification(offsetX, offsetY)
        val fragment = InfoFragment(::openNextScreen)
        fragment.arguments = Bundle().apply { putParcelable("model", marker.tag as? AirportsLocationResponse?) }

        val infoWindow = InfoWindow(marker, markerSpec, fragment)

        infoWindowManager.toggle(infoWindow, true)
        binding.root.postDelayed({
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.position, mMap.cameraPosition.zoom))
        }, 200L)
        return true
    }

    private fun openNextScreen() {
        if (isSingleTerminal)
            actionToCategoryFragment(terminals.first())
        else {
            actionToPassengerTerminalFragment(terminals)
        }
    }

    private fun actionToCategoryFragment(terminalResponse: TerminalResponse) {
        val action = HomeFragmentDirections.actionHomeFragmentToCategoryFragment(
            terminalResponse.id, terminalResponse.airportId.toInt(), ""
        )
        findNavController().navigate(action)
    }

    override fun onInfoWindowClick(marker: Marker) {
        // Todo need enhancement
    }


    private fun onBackPresses() {
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (binding.recyclerView.isVisible)
                        binding.recyclerView.isVisible = false
                    else
                        onAlertDialog()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    fun onAlertDialog() {
        val builder = AlertDialog.Builder(requireContext(), R.style.AlertDialogTheme)

        builder.setTitle(getString(R.string.exit_app))
        builder.setMessage(getString(R.string.do_want_exit))

        builder.setPositiveButton(getString(R.string.yes)) { _, _ -> activity?.finish() }

        builder.setNegativeButton(getString(R.string.no)) { _, _ -> builder.create() }

        builder.show()
    }
}







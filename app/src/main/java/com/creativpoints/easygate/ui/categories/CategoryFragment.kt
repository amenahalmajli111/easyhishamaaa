package com.creativpoints.easygate.ui.categories

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.CategoriesResponse
import com.creativpoints.easygate.data.model.airports.AirportsResponse
import com.creativpoints.easygate.databinding.FragmentCategoriesBinding
import com.creativpoints.easygate.ui.categories.adapter.CategoryAdapter
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class CategoryFragment : Fragment(), CategoryAdapter.OnItemClickListener {

    private lateinit var binding: FragmentCategoriesBinding
    private val viewModel: CategoryViewModel by viewModels()
    private lateinit var categoryAdapter: CategoryAdapter
    private val args by navArgs<CategoryFragmentArgs>()

    @Inject
    lateinit var prefs: Prefs


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentCategoriesBinding.inflate(layoutInflater)
        binding.prefs = prefs
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRequest()
        initAdapter()
        observer()
        initListener()
    }

    private fun initListener() {
        binding.imageViewBack.setOnClickListener(::onClickBackListener)
    }

    private fun onClickBackListener(view: View?) {
        requireActivity().onBackPressed()
    }

    private fun initRequest() {
        viewModel.callRemoteRequests(args.airportId)
        viewModel.getCategory(args.terminalId)
    }

    private fun initAdapter() {
        categoryAdapter = CategoryAdapter(this, prefs)
        binding.rvMenu.adapter = categoryAdapter
    }

    private fun observer() {

        viewModel.loadingLiveData.observe(viewLifecycleOwner, ::handleLoading)

        viewModel.errorLiveData.observe(viewLifecycleOwner, ::handleError)

        viewModel.successCategoryLiveData.observe(viewLifecycleOwner, ::categorySuccess)

        viewModel.successAirportLiveData.observe(viewLifecycleOwner, ::airportSuccess)

    }


    private fun categorySuccess(list: List<CategoriesResponse>) {
        categoryAdapter.submitList(list)
    }

    private fun airportSuccess(airportsResponse: AirportsResponse) {
        binding.model = airportsResponse
    }

    private fun handleLoading(flag: Boolean?) {
        binding.progressbar.isVisible = flag!!

    }

    fun handleError(exception: Exception) {
        FirebaseCrashlytics.getInstance().recordException(exception)


    }

    override fun onItemClick(categoriesResponse: CategoriesResponse) {
        if (categoriesResponse.id != 0) {

            val action = CategoryFragmentDirections.actionCategoryFragmentToAirlineTransitFragment(
                args.terminalId,
                categoriesResponse.id,
                "city",
                categoriesResponse.id,
                categoriesResponse.title(prefs)
            )
            findNavController().navigate(action)
        } else {
            val action =
                CategoryFragmentDirections.actionCategoryFragmentToCurrencyExchangeFragment()
            findNavController().navigate(action)


        }

    }

}

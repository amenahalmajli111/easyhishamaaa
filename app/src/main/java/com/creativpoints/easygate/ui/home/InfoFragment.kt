package com.creativpoints.easygate.ui.home

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.airports.AirportsLocationResponse
import com.creativpoints.easygate.databinding.InfoWindowFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class InfoFragment(private val onClickListener: ()->Unit) : Fragment() {

    private lateinit var binding: InfoWindowFragmentBinding



    @Inject
    lateinit var prefs: Prefs

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = InfoWindowFragmentBinding.inflate(layoutInflater)
        val view = binding.root
        view.post {
            view.layoutParams.width =
                Resources.getSystem().displayMetrics.widthPixels - Resources.getSystem().displayMetrics.widthPixels / 4
            view.requestLayout()
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val model = arguments?.getParcelable<AirportsLocationResponse>("model")


        binding.tvAirportName.text = model?.name(prefs)
        binding.tvAirportCity.text = model?.city(prefs)
        binding.root.setOnClickListener {
            onClickListener()
        }
    }



}

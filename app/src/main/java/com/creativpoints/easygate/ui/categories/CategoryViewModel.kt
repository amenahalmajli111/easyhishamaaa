package com.creativpoints.easygate.ui.categories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creativpoints.easygate.data.model.CategoriesResponse
import com.creativpoints.easygate.data.model.airports.AirportsResponse
import com.creativpoints.easygate.data.repository.AirportsRepository
import com.creativpoints.easygate.data.repository.CategoryRepository
import com.creativpoints.easygate.data.repository.TimeZoneRepository
import com.creativpoints.easygate.data.repository.WeatherRepository
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(
    private val categoryRepository: CategoryRepository,
    private val airportsRepository: AirportsRepository,
    private val timeZoneRepository: TimeZoneRepository,
    private val weatherRepository: WeatherRepository,
) : ViewModel() {

    private val _loadingHomeLiveData = MutableLiveData(false)
    val loadingLiveData: LiveData<Boolean> = _loadingHomeLiveData

    private val _errorSHomeLiveData = MutableLiveData<Exception>()
    val errorLiveData: LiveData<Exception> = _errorSHomeLiveData

    private val _successCategoryLiveData = MutableLiveData<List<CategoriesResponse>>()
    val successCategoryLiveData: LiveData<List<CategoriesResponse>> = _successCategoryLiveData

    private val _successCategoriesByCityNameLiveData = MutableLiveData<List<CategoriesResponse>>()
    val successCategoriesByCityNameLiveData: LiveData<List<CategoriesResponse>> =
        _successCategoriesByCityNameLiveData

    private val _successAirportLiveData = MutableLiveData<AirportsResponse>()
    val successAirportLiveData: LiveData<AirportsResponse> = _successAirportLiveData


    fun callRemoteRequests(airportId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {

                val airport = airportsRepository.getById(airportId)

                val latitude = airport.airportDetails.latitude
                val longitude = airport.airportDetails.longitude
                airport.temp = weatherRepository.getWeather(latitude, longitude)
                airport.formatted = timeZoneRepository.getByLatLong(latitude, longitude)

                _successAirportLiveData.postValue(airport)

            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }

    }


    fun getCategory(terminalId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {
                _successCategoryLiveData.postValue(
                    categoryRepository.getCategoriesByTerminalId(
                        terminalId
                    )
                )
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }


}
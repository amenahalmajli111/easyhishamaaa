package com.creativpoints.easygate.ui.teminal

import androidx.lifecycle.*
import com.creativpoints.easygate.data.model.TerminalResponse
import com.creativpoints.easygate.data.repository.TerminalRepository
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject



@HiltViewModel
class TerminalViewModel @Inject constructor(
    private val terminalRepository: TerminalRepository,
) : ViewModel() {


    private val _loadingHomeLiveData = MutableLiveData(false)//todo name
    val loadingLiveData: LiveData<Boolean> = _loadingHomeLiveData

    private val _errorSHomeLiveData = MutableLiveData<Exception>()
    val errorLiveData: LiveData<Exception> = _errorSHomeLiveData

    private val _successTerminalLiveData = MutableLiveData<List<TerminalResponse>>()
    val successTerminalLiveData: MutableLiveData<List<TerminalResponse>> = _successTerminalLiveData


   fun getTerminal(airportId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {
                _successTerminalLiveData.postValue(terminalRepository.getByAirportId(airportId))
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
             _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }

}

package com.creativpoints.easygate.ui.home

import androidx.lifecycle.*
import com.creativpoints.easygate.data.model.TerminalResponse
import com.creativpoints.easygate.data.model.airports.AirportsLocationResponse
import com.creativpoints.easygate.data.model.airports.AirportsResponse
import com.creativpoints.easygate.data.repository.AirportsRepository
import com.creativpoints.easygate.data.repository.TerminalRepository
import com.creativpoints.easygate.util.SingleLiveEvent
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val airportsRepository: AirportsRepository,
    private val terminalRepository: TerminalRepository, //todo
) : ViewModel(), LifecycleObserver {
    init {
        getLocation()
    }

    private val _loadingHomeLiveData = MutableLiveData<Boolean>()
    val loadingLiveData: LiveData<Boolean> = _loadingHomeLiveData

    private val _errorSHomeLiveData = MutableLiveData<Exception>()
    val errorLiveData: LiveData<Exception> = _errorSHomeLiveData

    private val _successLocationLiveData = MutableLiveData<List<AirportsLocationResponse>>()
    val successCLocationLiveData: LiveData<List<AirportsLocationResponse>> = _successLocationLiveData

    private val _successLocationSearchLiveData = MutableLiveData<List<AirportsResponse>>()
    val successLocationSearchLiveData: LiveData<List<AirportsResponse>> = _successLocationSearchLiveData


    private val _successTerminalLiveData = SingleLiveEvent<Pair<List<TerminalResponse>, Boolean>>()
    val successTerminalLiveData: SingleLiveEvent<Pair<List<TerminalResponse>, Boolean>> = _successTerminalLiveData

    fun getLocation() {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {
                _successLocationLiveData.postValue(airportsRepository.getAirportsLocation())
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }

    fun getSearchAirport(it: String) {
        viewModelScope.launch(Dispatchers.IO) {

            _loadingHomeLiveData.postValue(true)
            try {
                _successLocationSearchLiveData.postValue(airportsRepository.getBySearch(it))
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }

    fun getTerminal(airportId: Int, isOpenNextScreen: Boolean = false) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {
                _successTerminalLiveData.postValue(Pair(terminalRepository.getByAirportId(airportId), isOpenNextScreen))

            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
                _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }


}
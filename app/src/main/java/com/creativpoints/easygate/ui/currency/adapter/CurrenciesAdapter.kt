package com.creativpoints.easygate.ui.currency.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.creativpoints.easygate.data.model.local.CurrencyModel
import com.creativpoints.easygate.databinding.RowCurrencyBinding

class CurrenciesAdapter(
    private val currencies: List<CurrencyModel>
) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val viewHolder: ViewHolder
        if (view?.tag != null) {
            viewHolder = view.tag as ViewHolder
        } else {
            val binding = RowCurrencyBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
            view = binding.root
            viewHolder = ViewHolder(binding)
            view.tag = viewHolder
        }
        viewHolder.bind(position)
        return view
    }

    override fun getItem(position: Int): Any {
        return currencies[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return currencies.size
    }

    private inner class ViewHolder(private val binding: RowCurrencyBinding) {

        fun bind(position: Int) {
            val currency = currencies[position]
            binding.imageViewRowRateFlag.setImageResource(currency.flag)
            binding.currency = currency
            binding.executePendingBindings()
        }
    }
}

package com.creativpoints.easygate.ui.airline.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.creativpoints.easygate.data.model.article.ArticleInfoResponse
import com.creativpoints.easygate.databinding.RowAirlineTransitBinding

class AirlineTransitAdapter(val listener: OnItemClickListener) :
    ListAdapter<ArticleInfoResponse, AirlineTransitAdapter.AirlineViewHolder>(DiffCallback()) {

    inner class AirlineViewHolder(private val binding: RowAirlineTransitBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(articleInfoResponse: ArticleInfoResponse) {
            binding.model = articleInfoResponse
            binding.executePendingBindings()
            binding.constraint.setOnClickListener {
                listener.onItemClick(articleInfoResponse)
            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AirlineViewHolder {
        val binding =
            RowAirlineTransitBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AirlineViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AirlineViewHolder, position: Int) {
        val article = getItem(position)
        holder.bind(article)
    }


    class DiffCallback : DiffUtil.ItemCallback<ArticleInfoResponse>() {
        override fun areItemsTheSame(
            oldItem: ArticleInfoResponse,
            newItem: ArticleInfoResponse,
        ): Boolean {
            return oldItem.id == newItem.id

        }

        override fun areContentsTheSame(
            oldItem: ArticleInfoResponse,
            newItem: ArticleInfoResponse,
        ): Boolean {
            return oldItem == newItem
        }

    }

    interface OnItemClickListener {
        fun onItemClick(articleInfoResponse: ArticleInfoResponse)
    }
}
package com.creativpoints.easygate.ui.currency

import android.content.Context
import androidx.lifecycle.*
import com.creativpoints.easygate.R
import com.creativpoints.easygate.data.model.local.CurrencyModel
import com.creativpoints.easygate.data.repository.CurrencyRepository
import com.creativpoints.easygate.util.toDrawableResource
import com.creativpoints.easygate.util.toStringResource
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CurrencyExchangeViewModel @Inject constructor(
    private val currencyRepository: CurrencyRepository,
    @ApplicationContext private val context: Context
) : ViewModel(), LifecycleObserver {


    private val _loadingHomeLiveData = MutableLiveData(false)
    val loadingLiveData: LiveData<Boolean> = _loadingHomeLiveData

    private val _errorSHomeLiveData = MutableLiveData<Exception>()
    val errorLiveData: LiveData<Exception> = _errorSHomeLiveData

    private val _successCurrencyLiveData = MutableLiveData<String>()
    val successCurrencyLiveData: LiveData<String> = _successCurrencyLiveData


    fun getCurrency(from: String, to: String, amount: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _loadingHomeLiveData.postValue(true)
            try {

                val result = currencyRepository.getConvert(from, to, amount)
                val format = NumberFormat.getInstance(Locale.ENGLISH)
                val toResult = format.format(result)
                val amountConverted = String.format(Locale.ENGLISH, "%s %s", toResult, to)

                _successCurrencyLiveData.postValue(amountConverted)
            } catch (e: Exception) {
                FirebaseCrashlytics.getInstance().recordException(e)
             _errorSHomeLiveData.postValue(e)
            }
            _loadingHomeLiveData.postValue(false)
        }
    }


    fun getCurrencies(): List<CurrencyModel> {
        val stringCurrencies = context.resources.getStringArray(R.array.currencies)
        val currencies = mutableListOf<CurrencyModel>()
        stringCurrencies.forEach {
            currencies.add(
                CurrencyModel(
                    iso = it,
                    name = "currency_%s_name".format(it.lowercase(Locale.ENGLISH)).toStringResource(context),
                    flag = "ic_currency_%s_flag".format(it.lowercase(Locale.ENGLISH)).toDrawableResource(context)
                )
            )
        }
        return currencies
    }


}
package com.creativpoints.easygate.ui.categories.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.creativpoints.easygate.common.prefs.Prefs
import com.creativpoints.easygate.data.model.CategoriesResponse
import com.creativpoints.easygate.databinding.RowCategotiesBinding

class CategoryAdapter(val listener: OnItemClickListener, val prefs: Prefs) :
    ListAdapter<CategoriesResponse, CategoryAdapter.CategoryViewHolder>(DiffCallback()) {


    inner class CategoryViewHolder(private val binding: RowCategotiesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(categoriesResponse: CategoriesResponse) {
            binding.prefs = prefs
            binding.model = categoriesResponse
            binding.executePendingBindings()
            binding.constraint.setOnClickListener {
                listener.onItemClick(categoriesResponse)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding =
            RowCategotiesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val airport = getItem(position)
        holder.bind(airport)
    }


    class DiffCallback : DiffUtil.ItemCallback<CategoriesResponse>() {
        override fun areItemsTheSame(
            oldItem: CategoriesResponse,
            newItem: CategoriesResponse,
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: CategoriesResponse,
            newItem: CategoriesResponse,
        ): Boolean {
            return oldItem == newItem
        }

    }

    interface OnItemClickListener {
        fun onItemClick(categoriesResponse: CategoriesResponse)
    }
}
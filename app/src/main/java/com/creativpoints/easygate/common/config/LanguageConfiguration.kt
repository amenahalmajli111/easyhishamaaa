package com.creativpoints.easygate.common.config

import android.content.Context
import android.content.res.Configuration
import androidx.preference.PreferenceManager
import java.util.*
import javax.inject.Inject

class LanguageConfiguration @Inject constructor() {

    fun getLocalizedContext(context: Context): Context {
        return updateResources(context, getAppLanguage(context).value)
    }

    private fun getAppLanguage(context: Context): Language {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val language = sharedPreferences.getString("LANGUAGE", null)
        return if (language != null) {
            Language.getByValue(language)
        } else {
            Language.getByValue(Locale.getDefault().language)
        }
    }

    private fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language, "KW")
        Locale.setDefault(locale)

        val config = Configuration(context.resources?.configuration)
        config.setLocale(locale)

        return context.createConfigurationContext(config)
    }

    enum class Language(val value: String) {

        ENGLISH("en"),

        ARABIC("ar");

        companion object {

            fun getByValue(value: String): Language {
                return values().first { it.value == value }
            }
        }
    }
}

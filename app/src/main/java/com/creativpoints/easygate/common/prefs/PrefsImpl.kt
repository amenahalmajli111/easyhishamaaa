package com.creativpoints.easygate.common.prefs

import android.content.SharedPreferences
import androidx.core.content.edit

class PrefsImpl(
    private val sharedPreferences: SharedPreferences,
) : Prefs {


    override var language: String
        get() = sharedPreferences.getString(PrefKey.LANGUAGE.name, "en") ?: "en"
        set(value) {
            sharedPreferences.edit { putString(PrefKey.LANGUAGE.name, value) }
        }

    private enum class PrefKey {

        LANGUAGE

    }
}
